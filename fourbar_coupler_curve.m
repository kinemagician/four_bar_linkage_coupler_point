function [coupler,itype] = fourbar_coupler_curve(X1,X2,X3,X4,A0,B0,T1,T2,T3,T4,ac,alpha,IFLAG,Npos,dtheta)
%FOURBAR: Compute the coordinattes of the coupler curve
% INPUT parameters
% X1,X2,X3,X4: Link lengtths
% T1: Angular position of input link adjacent to the left of link frame
% T2: Coupler angular position
% T3: Angular position of input link opposite to the left of link frame 
% T4: Fixed angular position of frame link
% ac,alpha: Polar coordinates with respect joint in A
%  IFLAG: Branch Id of the four-bar 
% Npos: Number of positions
% dtheta: Input link step increment (degrees)
%  OUTPUT parameters
%  coupler: array Npos x 2 with coupler point curve coordinates
coupler=[];
theta=T4-pi;
% Crank rocker (X1 crank)
lengths=[X1,X2,X3,X4];
xmax=max(lengths);
xmin=min(lengths);
idmin = find(lengths == xmin); % Get position of minimum length
idmax = find(lengths == xmax); % Get position of maximum length

lengths2=[];
lengths2(idmin)=xmin;
lengths2(idmax)=xmax;
lengths3=lengths-lengths2;
 if and(X1==xmin,xmin+xmax<=sum(lengths3))
    disp('Grashof crank-rocker')
    itype='Grashof crank-rocker';
    for i=1:Npos
[T2,T3,IFLAG2]=fourbarpos(X1,X2,X3,X4,T1,T4,IFLAG);
[pt2] = couplerpointcords(A0,theta,X1,X2,X3,X4,T1,T2,T3,T4,ac,alpha);
T1=T1+deg2rad(dtheta);
coupler=[coupler;pt2];    
    end
    return
end
% Crank rocker (X3 crank)
if and(X3==xmin,xmin+xmax<=sum(lengths3))
    itype='Grashof crank-rocker';
       disp('Grashof crank-rocker')
    for i=1:Npos
[T2,T1,IFLAG2]=fourbarpos(X3,X2,X1,X4,T3,T4,IFLAG);
[pt2] = couplerpointcords(A0,theta,X1,X2,X3,X4,T1,T2,T3,T4,ac,alpha);
T3=T3+deg2rad(dtheta);
coupler=[coupler;pt2];    
    end
    return
end
% Grashof double rocker (X2 crank)
if and(X2==xmin,xmin+xmax<=sum(lengths3))    
    disp('Grashof double rocker')    
    itype='Grashof double rocker';
        for i=1:Npos
[T1,T3,IFLAG2]=fourbarpos(X2,X1,X3,X4,T2,T4,IFLAG);
[pt2] = couplerpointcords(A0,theta,X1,X2,X3,X4,T1,T2,T3,T4,ac,alpha);
T2=T2+deg2rad(dtheta);
coupler=[coupler;pt2]; 
        end
        return
end
%
% Non Grashof linkage
%  compute extreme values of angle T1

    disp('Non Grashof double rocker')
    itype='Non Grashof double rocker'
r=X2+X3;
T1_max=acos((X1^2+X4^2-r^2)/(2*X1*X4))+angle2Points(A0,B0);
T1_min=acos((X1^2+X4^2-r^2)/(2*X1*X4))-angle2Points(A0,B0);
theta1v = linspace(-T1_min,T1_max,Npos);
coupler1=[];
    for i=1:Npos  % Plot first branch
T1=theta1v(i);  
[T2,T3,IFLAG2]=fourbarpos(X1,X2,X3,X4,T1,T4,1); 
[pt2] =couplerpointcords(A0,theta,X1,X2,X3,X4,T1,T2,T3,T4,ac,alpha);
coupler1=[coupler1;pt2];
    end  
coupler2=[];
    for i=1:Npos  % Plot second branch
T1=theta1v(i); 
[T2,T3,IFLAG2]=fourbarpos(X1,X2,X3,X4,T1,T4,2); 
[pt2] = couplerpointcords(A0,theta,X1,X2,X3,X4,T1,T2,T3,T4,ac,alpha);
coupler2=[coupler2;pt2];
    end
coupler=[coupler1;flipud(coupler2)];


end

