function varargout = fourbarpos2(varargin)
%FOURBARPOS2 Draw a four-bar linkage and
% compute link lengths and angular positions
% INPUT PARAMETERS
% Fixed revolute pairs coordinates
% varargin{1}=A0
% varargin{2}=B0
% Moving revolute pairs coordinates
% varargin{3}=A
% varargin{4}=B
% Coupler point
% varargin{5}=C
% Drawing parameters
% varargin{6}=1  Draw four-bar linkage
% varargin{7}=R  Radius of the revolute pairs circles
% OUTPUT PARAMETERS
% X1,X2,X3,X4: Link lengths
% T1,T2,T3,T4: link angles
% Coupler polar distance: AC,alpha
% branch number for position analysis: IFLAG
eps1=1.e-5;
    A0 = varargin{1};
    A = varargin{2};
    B = varargin{3};
    B0 = varargin{4};
    T1= angle2Points(A0,A);
    T2= angle2Points(A,B);
    T3=angle2Points(B,B0);
    T4=angle2Points(B0,A0);
    X1=distancePoints(A0,A);
    X2=distancePoints(A,B); 
    X3=distancePoints(B,B0);
    X4=distancePoints(A0,B0);
    [T2a,T3a,IFLAG2]=fourbarpos(X1,X2,X3,X4,T1,T4,1);
    [T2b,T3b,IFLAG2]=fourbarpos(X1,X2,X3,X4,T1,T4,2);
    t1a=abs(T2-T2a)<eps1;
    t2a=abs(T3-T3a)<eps1;
    t3a=abs(T2-T2b)<eps1;
    t4a=abs(T3-T3b)<eps1;

    if and(t1a,t2a)
        IFLAG=round(1);
    end
    if and(t3a,t4a)
        IFLAG=round(2);
    end


    switch nargin
        case 7 % Draw a four-bar with coupler point 
    C = varargin{5};
    idraw = varargin{6};
    R=varargin{7};
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    alpha=angle2Points(A,C)-T2;
    ac=distancePoints(A,C);
    figure
            A0A=drawEdge(A0,A);
            hold on
            AB=drawEdge(A,B);
            BB0=drawEdge(B,B0);
            B0A0=drawEdge(B0,A0,'linewidth',2);
            AC=drawEdge(A,C);           
            BC=drawEdge(B,C);
            drawCircle(A0, R);
            drawCircle(B0, R);
            drawCircle(A, R);
            drawCircle(B, R);
            drawCircle(C, 0.5*R);
            axis equal
        end
    varargout={X1,X2,X3,X4,T1,T2,T3,T4,ac,alpha,IFLAG};  
end

