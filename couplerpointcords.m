function [pt1] = couplerpointcords(A0,theta,X1,X2,X3,X4,T1,T2,T3,T4,ac,alpha)
%COUPLERPOINTCORDS Compute and tranform coupler point coordinates
% requires geom2d package
xc=A0(1)+X1*cos(T1)+ac*cos(T2+alpha);
yc=A0(2)+X1*sin(T1)+ac*sin(T2+alpha);
pt1=[xc,yc];
end

