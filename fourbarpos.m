function  [T2,T3,IFLAG2]=fourbarpos(X1,X2,X3,X4,T1,T4,IFLAG)
% Author: Ettore Pennestri' pennestri@mec.uniroma2.it
% v0.1
% 
%    PURPOSE: Position analysis of plane four-bar linkage.
% 
% INPUT PARAMETERS:
% C X1, X2, X3, X4 : Link lengths.  (REAL)
%  T1 : Input angle. (REAL) 
%  T4 : Frame fixed angle. (REAL) (If X-axis lies on X4  T4=PI rad
%  IFLAG : =1 Not crossed linkage
%          =2 Crossed linkage   (INTEGER)
% OUTPUT PARAMETERS:
%  T2 : Coupler angle.  (REAL)
%  T3 : Output angle. (REAL)
%  IFLAG2=0: Feasible position. 
%  IFLAG2=1: Not feasible position has been reached. 
%   All the angles are expressed in radians
A=2*X2*(X1*sin(T1)+X4*sin(T4));
B=2*X2*(X1*cos(T1)+X4*cos(T4));
C=X1^2+X2^2-X3^2+X4^2+2*X1*X4*(cos(T1)*cos(T4)+sin(T1)*sin(T4));
QUA=A^2+B^2-C^2;
if QUA< 0 % Imaginary roots 
IFLAG2=1;
T3=0.;
T2=0.;
return;
end
QUA=sqrt(QUA);
if(IFLAG==2)
    TANET=(-A-QUA)/(C-B);
end
if(IFLAG==1)
    TANET=(-A+QUA)/(C-B);
end
T2=2*atan(TANET);
if T2<0
    T2=T2+2*pi;
end
AUX=-(X1*cos(T1)+X2*cos(T2)+X4*cos(T4))/X3;
AUX1=-(X1*sin(T1)+X2*sin(T2)+X4*sin(T4))/X3;
T3=atan2(AUX1,AUX);
IFLAG2=0;
if T3<0
    T3=T3+2*pi;
end
end

