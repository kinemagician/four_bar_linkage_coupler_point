%
% Plotting of a four-bar linkage coupler curve
%
clc
close all
clear all

% OK
% A0=[0,0]
% A=[1.5,3]
% B=[2.5,3]
% B0=[6.,0]
% C=[2,3]

% OK
% A0=[0,0]
% A=[1.5,3]
% B=[2.5,3.2]
% B0=[6.,1]
% C=[2,3]

%ok
%  A0=[1,1]
%   A=A0+[1.,2]
%  B=A0+[5,5]
%  B0=A0+[7.,0.5]
% % 
%  C=A0+[1,4]
% Specify revolute joints center coordinnates
% ok
 A0=[1,0];
 A=A0+[1.,2];
 B=A0+[5,5];
 B0=A0+[12.,1];
 % Specify coupler point coordinates
 C=A0+[1,4];
 % Position analysis and drawing of the four-bar linkage
 [X1,X2,X3,X4,T1,T2,T3,T4,ac,alpha,IFLAG]=fourbarpos2(A0,A,B,B0,C,1,0.05);
 Npos=360;  % Number of positions
 dtheta=1;  % Input link angular step motion (degrees)
 [coupler,itype] = fourbar_coupler_curve(X1,X2,X3,X4,A0,B0,T1,T2,T3,T4,ac,alpha,IFLAG,Npos,dtheta);
 % Draw coupler curve 
 drawPolyline(coupler);